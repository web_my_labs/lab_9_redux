import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { AppRoutes } from './common/constants';
import { Home, Post, Profile } from './pages';
import { Provider } from 'react-redux';
import { store } from './store';

function App() {
  /**
   * Таким образом подключается стор к дереву компонентов.
   *  Создается Provider с store-ом в пропсах
   */
  return (
    <Provider store={store}>
      <div>
        <header>
          <a href={AppRoutes.HOME}>
            <h1>Awesome Posts 🤓</h1>
          </a>
        </header>
        <BrowserRouter>
          <Routes>
            <Route path={`${AppRoutes.POSTS_BASE}/:id`} element={<Post />} />
            <Route
              path={`${AppRoutes.PROFILES_BASE}/:id`}
              element={<Profile />}
            />
            <Route path="/" element={<Home />} />
          </Routes>
        </BrowserRouter>
      </div>
    </Provider>
  );
}

export default App;
