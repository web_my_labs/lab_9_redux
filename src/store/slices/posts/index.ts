import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { RootState } from '../..';
import { getManyPosts, getSinglePost } from '../../../api';
import { FetchStatus, Post } from '../../../common/types';

/**
 * Опишем начальное состояние. Обратите внимание как через оператор as массив
 *  [] приводится к типу Post[]
 */
const initialState = {
  //Объект, ключ -- айди поста, значение -- статус загрузки поста
  postsLoadingMap: {} as Record<number, FetchStatus>,
  //Статус загрузки всех постов
  allPostsFetchStatus: null as FetchStatus | null,
  posts: [] as Post[],
};

/**
 * Создаем санку на загрузку всех постов
 */
const fetchPosts = createAsyncThunk<Post[], unknown>(
  'posts/fetchAll',
  async (): Promise<Post[]> => {
    const posts = await getManyPosts();
    return posts;
  }
);

/**
 * Создаем санку на загрузку одного поста
 * Обратите внимание на дженерики. Первый дженерик-тип -- тип значения
 *  возвращаемого из санки. Второй дженерик-тип -- тип аргумента, который
 *  принимает санка
 * */
const fetchSinglePost = createAsyncThunk<Post, number>(
  'posts/fetchOne',
  async (postId: number): Promise<Post> => {
    /**
     * По аналогии с fetchPosts, только для одного поста
     */
  }
);

export const slice = createSlice({
  name: 'posts',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    //Опишите редусеры для состояния санки fetchPosts
    builder.addCase(fetchPosts.pending, (state, _) => {
      /** */
    });
    builder.addCase(fetchPosts.rejected, (state, _) => {
      /** */
    });
    builder.addCase(fetchPosts.fulfilled, (state, { payload }) => {
      /** */
      /** */
    });
    builder.addCase(fetchSinglePost.pending, (state, { meta }) => {
      state.postsLoadingMap[meta.arg] = 'pending';
    });
    builder.addCase(fetchSinglePost.rejected, (state, { meta }) => {
      state.postsLoadingMap[meta.arg] = 'rejected';
    });
    builder.addCase(fetchSinglePost.fulfilled, (state, { meta, payload }) => {
      state.postsLoadingMap[meta.arg] = 'fulfilled';
      state.posts = [...state.posts, payload];
    });
  },
});

/**
 * Допишите нужные селекторы и не забудьте экспортировать их
 */
const selectAllPosts = (state: RootState) => state.posts.posts;
/**
 * Остальные селекторы
 */

/**
 * Экспортируем экшены и селекторы.
 * Таким образом мы группируем экшены в объект actions, а селекторы
 *  в объект selectors
 */
export const actions = { ...slice.actions, fetchPosts, fetchSinglePost };
export const reducer = slice.reducer;
export const selectors = {
  selectAllPosts,
};
